#!/usr/bin/env python3
"""
Set your Zulip status based on location (RouterMAC)
"""

import os
import zulip
import configparser

## author: Tilman Beitter

__license__ = "GPLv3+"

def get_status(client):
    presence = client.get_user_presence(client.get_profile()['email'])
    aggregated_status = presence['presence']['aggregated']['status']
    return aggregated_status

def get_message(client, zuliprc):

    # first prepare config
    config = configparser.ConfigParser(delimiters=('=',))
    config.read(zuliprc)

    locations_dict = {}

    for key, value in config.items('locations'):
        locations_dict[key] = value        

    # check if a known mac address can be found and set the message
    with open('/proc/net/arp', 'r') as f:
        for line in f:
            fields = line.split()
            if fields[3] in locations_dict:
                status_message = locations_dict[fields[3]]
                return status_message
                break

def set_status(client, status_message):
    request = {
        "status_text": status_message
    }
    result = client.call_endpoint(url="/users/me/status", method="POST", request=request)
 
    if result['result'] != "success":
        print ("Error: %s - %s" % (result['status_code'], result['msg']))
        exit(1)

def main() -> int:

    # We need a config 
    zuliprc = os.path.expanduser("~/.zuliprc")
    if not os.path.exists(zuliprc):
        sys.exit("Error: Config file %s not found." % (zuliprc))   

    # Init the zulip client
    client = zulip.Client(config_file=zuliprc)

    # Do not set a status while offline
    if get_status(client) == "offline":
        print("No client online. Exiting.")    
        return 0

    status_message = get_message(client, zuliprc)
    if status_message:
        set_status(client, status_message)
        print("Successfully set:", status_message)
    else:
        print("No known mac address found")

if __name__ == "__main__":
    try:
        exit(main())
    except KeyboardInterrupt:
        # Called in case of Ctrl+c
        print("Goodbye")
