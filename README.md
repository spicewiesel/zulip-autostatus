# Zulip Autostatus

## About

This is a simple python script that will set your Zulip status based on location (== known Router MACs).

## Installation

### ArchLinux

Build your package locally:

```
git clone https://gitlab.com/spicewiesel/zulip-autostatus.git
cd zulip-autostatus/pkg
makepkg -s
```

Afterwards just install the just build .zst file:

```
sudo pacman -U zulip-autostatus-[...].pkg.tar.zst
```

### Manual install

```
git clone https://gitlab.com/spicewiesel/zulip-autostatus.git
cd zulip-autostatus
cp pkg/zulip-autostatus.service ~/config/systemd/user/
cp pkg/zulip-autostatus.timer ~/config/systemd/user/
sudo cp zulip-autostatus.py /usr/share/zulip-autostatus/
sudo ln -sf /usr/share/zulip-autostatus/zulip-autostatus.py /usr/bin/zulip-autostatus.py
systemctl --user daemon-reload
systemctl --user enable --now zulip-autostatus.timer
```

