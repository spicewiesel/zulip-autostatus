from setuptools import setup

setup(
    name='zulip-autostatus',
    version='0.1',
    install_requires=[
        'zulip',
    ],
)
